# Mail Sender

Ce logiciel a été conçu dans le cadre du cours de Conception Logicielle de deuxième année à l'ENSAI.

## Présentation

Il s'agit d'un logiciel permettant de programmer manuellement l'envoi de mails, conçu sous la forme d'un webservice avec lequel l'utilisateur communique via une interface indépendante. Attention, ce n'est PAS un logiciel de spam, aussi il n'est pas conçu pour envoyer facilement un grand nombre de mails.

## Procédure d'installation 

### Prérequis
Pour utiliser cette application, vous devez disposer:
- D'un environnement qui permet l'accès à `python` ainsi qu'à `pip`.
- D'un accès à système de base de donnée hebergé en local ou distant. (type pgAdmin4 - PostrgresSQL)
- D'un accès à un serveur SMTP (une adresse mail...)

### Installation
Pour installer l'apllication, cloner ce dépôt : 

```
git clone https://gitlab.com/Koorido/mail-sender.git
cd mail-sender
```

Puis installer les dépendances : 
```
pip install -r requirements.txt
```

### Configuration

Dans le fichier `.env`, renseigner vos paramètres:

```python
#Database parameters
DB_PASSWORD=                            #Mot de passe de l'utilisateur
DB_HOST=localhost                       #Adresse et numéro de port du serveur SQL
DB_PORT=5433        
DATABASE=mailsender                     #Nom du schéma de base de données
DB_USER=postgres                        #Nom du l'utilisateur ayant un accès en READ et WRITE

WEBSERVICE_HOST=http://localhost:8000/  #Dans une utilisation normale, il n'est pas nécessaire de modifier cela

SMTP_PORT=587
SMTP_HOST=smtp-mail.outlook.com         #Nom du serveur SMTP (gmail : smtp.gmail.com; yahoo : smtp.mail.yahoo.com, ...)
SMTP_PASSWORD=                          #Mot de passe lié à l'adresse utilisée pour l'envoi de mails
```

### Exécution

Deux scripts python sont à lancer pour démarrer l'application:

- `main.py` démarre le webservice ainsi que l'envoi de mail:
```
cd mail-sender
python main.py
```

- Dans une console séparée, `home.py` lance l'interface graphique: 
```
cd mail-sender
python home.py
```

## Fonctionnement et utilisation

Depuis la fenêtre d'accueil, l'utilisateur peut écrire un mail et l'envoyer. Il doit renseigner sa propre adresse mail, celle du destinataire, l'heure d'envoi désirée et le contenu du mail. Un message l'avertit que sa demande a bien été prise en compte. Il a également accès à une deuxième fenêtre qui lui permet de visualiser toutes les demandes passées. Il peut inspecter chacune de ces demandes, et plusieurs options s'offrent à lui: 

- Si le mail sélectionné n'a pas encore été envoyé, il peut choisir d'annuler l'envoi, ou de modifier sa demande (date, message, ...). Dans le deuxième cas, l'ancienne version de la demande est effacée de l'historique.
- Si le mail sélectionné a déjà été envoyé, il peut choisir de le supprimer de l'historique, ou de le renvoyer, éventuellement après avoir modifié certains éléments.

L'interface graphique contenue dans le dépôt est totalement indépendante du reste de l'application et communique uniquement avec le webservice par requête HTTP. 

### Points d'attention:

Les entrées sont vérifiées automatiquement, mais certaines précautions sont de mise:
- Un mail ne peut pas être programmé à moins d'une minute.
- L'objet du mail, ainsi que le message doivent être écrits uniquement avec des caractères ASCII (notamment, pas d'accents)

## Description de l'API

### Points de terminaison GET

- `/demande/{id}`: Renvoie le contenu de la demande correspondant à l'id renseigné.

- `/demandes` : Renvoie l'intégralité des demandes effectuées.

- `/demandes/pending` : Renvoie toutes les demandes en attente. (i.e. non envoyées)

- `/demande/{id}/drop` : Supprime la demande correspondant à l'id renseigné.

### Points de terminaison POST

- `/demande`: utilisé pour poster une demande d'envoi. Le contenu doit être un JSON formatté comme suit : 

```python
{
    "from_address" : ,   # adresse mail de l'expéditeur
    "to_address" : ,     # adresse mail du destinataire
    "mail_subject" : ,    # Objet du mail
    "mail_text" : ,      # Contenu du mail (ASCII)
    "desired_date" :     # Date d'envoi désirée, format "YYYY-MM-DDTHH:MM:SS" (ou datetime)
}
```
Retourne l'id correspondant à la demande.

- `/setstatus` : Technique, modifie le statut d'un mail, désigné par son id. Le contenu doit être un JSON formatté comme suit :

```python
{
    "id_mail" : ,         #int
    "status_code" : }     #int
```