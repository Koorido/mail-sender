from fastapi import FastAPI
from utils.post_objects import Mail, StatusUpdate
from dao.mail_dao import MailDAO

# Any given endpoint is linked to a MailDAO() method. To get more information about what they're doing 
# see dao.mail_dao.MailDAO documentation

app = FastAPI()

@app.post("/demande")
async def new_mail(mail : Mail):
    id_created = MailDAO().add_mail(**mail.dict())
    return {"status" : "SUCCESS",
    "id_mail": id_created}

@app.get("/demandes/")
async def get_all_inquiries():
    return MailDAO().get_all_mail()

@app.get("/demande/{demande_id}/")
async def get_inquiry_by_id(demande_id : int):
    return MailDAO().get_mail_by_id(demande_id)


#Endpoints supplémentaires

@app.get("/demande/{demande_id}/drop")
async def delete_by_id(demande_id : int):
    return MailDAO().delete_mail_by_id(demande_id)

@app.get("/demandes/pending")
async def get_pending_inquiries():
    return MailDAO().get_pending_mail()

@app.post("/setstatus")
async def set_status(status : StatusUpdate):
    return MailDAO().set_status(**status.dict())
    