import os
import dotenv
import psycopg2
from psycopg2.extras import RealDictCursor

from utils.singleton import Singleton

class DBConnection(metaclass=Singleton):
    """ Technical class to ensure only one connection to the database is opened at a time"""

    def __init__(self):
        dotenv.load_dotenv(override=True)
        self.__connection = psycopg2.connect(
            host=os.environ["DB_HOST"],
            port=os.environ["DB_PORT"],
            database=os.environ["DATABASE"],
            user=os.environ["DB_USER"],
            password=os.environ["DB_PASSWORD"],
            cursor_factory=RealDictCursor)

    @property
    def connection(self):
        """ Return the opened connection """
        return self.__connection
