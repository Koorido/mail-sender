from dao.db_connection import DBConnection
# from dao.to_send_dao import ToSendDAO

class MailDAO:
    """Implements several methods to read/update the database"""

    def get_mail_by_id(self, mail_id : int):
        """Return one mail refered as by its id

        Parameters
        ----------
        mail_id : int

        Returns
        -------
        dict
            Keys : 'id_mail', 'from_addr', 'to_addr', 'mail_subject', 'mail_text', 'mail_status'
        """
        query = "SELECT * FROM mail_storage WHERE id_mail = %(id_mail)s;"
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    query,
                    {"id_mail": mail_id}
                )
                res = cursor.fetchone()
        return res
    
    def get_all_mail(self):
        """Returns the whole database
        
        Returns
        -------
        dict list
        """
        query = "SELECT * FROM mail_storage ORDER BY id_mail DESC;"
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    query
                )
                res = cursor.fetchall()
        return res
    
    def add_mail(self, from_address, to_address, mail_subject, mail_text, desired_date):
        """Adds a new mail to the database

        As it is a new mail, its status is 'NOT_SENT_YET', and doesn't has to be specified
        Parameters
        ----------
        from_address : str
            A valid email address (aaa.bbb@name.xxx)
        to_address : str
            A valid email address
        mail_subject : str
            Mail subject, ASCII encoding
        mail_text : str
            Mail content, ASCII encoding
        desired_date : str
            Format '2020-05-20 14:30:00'
        Returns
        -------
        int
            New mail's id
        """
        query = "INSERT INTO mail_storage(from_addr, to_addr, mail_subject, mail_content,mail_date, mail_status) VALUES" \
                    "(%(from_addr)s, %(to_addr)s, %(mail_sbj)s, %(mail_content)s, %(mail_date)s, %(mail_status)s) RETURNING id_mail;"
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    query,
                    {"from_addr" : from_address,
                    "to_addr" : to_address,
                    "mail_sbj" : mail_subject, 
                    "mail_content" : mail_text, 
                    "mail_date" : desired_date,
                    "mail_status" : "NOT_SENT_YET"}
                )
                res = cursor.fetchone()
        return res["id_mail"]

    def delete_mail_by_id(self, id_mail):
        """Deletes a mail from the database"""

        query = "DELETE FROM mail_storage WHERE id_mail = %(id_mail)s RETURNING mail_status, id_mail;"
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    query,
                    {"id_mail" : id_mail}
                )
                res = cursor.fetchone()
        return {"id_mail_deleted" : res['id_mail'],
                    "status" : "Mail deleted"}
    
    def get_pending_mail(self):
        """Returns every mail with 'NOT_SENT_YET' status
        
        Returns
        -------
        dict list
        """
        query = "SELECT * FROM mail_storage WHERE mail_status = 'NOT_SENT_YET';"
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    query
                )
                res = cursor.fetchall()
        return res
    
    def set_status(self, id_mail, status_code):
        """Updates mail status

        Parameters
        ----------
        id_mail : int
            Mail id
        status_code : int
            0 or 1, 0 meaning 'NOT_SENT' and 1 meaning 'SENT'
        """
        query = "UPDATE mail_storage SET mail_status = %(status_label)s WHERE id_mail = %(id_mail)s RETURNING mail_status"
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                        query,
                        {'id_mail' : id_mail,
                        'status_label' : 'SENT' if status_code == 1 else 'NOT_SENT'}
                    )
                res = cursor.fetchone()
        return res




    
   


