from unittest import TestCase, main as unittest_main
from dao.mail_dao import MailDAO

#Tests need the .env file to be complete.

class TestDAO(TestCase):
    MAIL_1 = {
    "from_address" : "test1.from@python.org",   
    "to_address" : "test1.to@python.org",     
    "mail_subject" : "This is test 1",   
    "mail_text" : "This is some text",    
    "desired_date" :  "2023-01-17 13:10:00"   
    }
    MAIL_2 = {
    "from_address" : "test2.from@python.org",   
    "to_address" : "test2.to@python.org",     
    "mail_subject" : "This is test 2",   
    "mail_text" : "This is some text again",    
    "desired_date" :  "2023-02-17 13:10:00"   
    }

    def test_delete_mail_by_id(self):
        #GIVEN
        id1 = MailDAO().add_mail(**TestDAO.MAIL_1)
        id2 = MailDAO().add_mail(**TestDAO.MAIL_2)
        #WHEN
        res1 = MailDAO().delete_mail_by_id(id1)
        res2 = MailDAO().delete_mail_by_id(id2)
        res3 = MailDAO().get_mail_by_id(id2)
        #THEN
        self.assertEqual(res1['id_mail_deleted'],id1)
        self.assertEqual(res2['id_mail_deleted'],id2)
        self.assertIsNone(res3)
    
    def test_get_all_mail(self):
        #GIVEN
        nb_mail_init = len(MailDAO().get_all_mail())
        #WHEN
        id1 = MailDAO().add_mail(**TestDAO.MAIL_1)
        id2 = MailDAO().add_mail(**TestDAO.MAIL_2)
        nb_mail_now = len(MailDAO().get_all_mail())
        res1 = MailDAO().delete_mail_by_id(id1)
        res2 = MailDAO().delete_mail_by_id(id2)
        #THEN
        self.assertEqual(nb_mail_init, nb_mail_now - 2)
    
    def test_add_mail(self):
        #GIVEN
        n = len(MailDAO().get_all_mail())
        #WHEN
        id1 = MailDAO().add_mail(**TestDAO.MAIL_1)
        n2 =  len(MailDAO().get_all_mail())
        #THEN 
        self.assertIsInstance(id1, int)
        self.assertEqual(n, n2 - 1)
        self.assertIsNotNone(MailDAO().delete_mail_by_id(id1))
    
    def test_set_status(self):
        #GIVEN 
        id1 = MailDAO().add_mail(**TestDAO.MAIL_1)
        #WHEN 
        res = MailDAO().set_status(id1, 0)
        MailDAO().delete_mail_by_id(id1)
        #THEN 
        self.assertEqual(res['mail_status'], 'NOT_SENT')
        
    
    
if __name__ == '__main__':
    unittest_main()
