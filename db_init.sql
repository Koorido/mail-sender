DROP SEQUENCE IF EXISTS mail_seq CASCADE;

DROP TABLE IF EXISTS mail_storage CASCADE;

CREATE SEQUENCE mail_seq;
CREATE TABLE mail_storage (
    id_mail INT PRIMARY KEY DEFAULT nextval('mail_seq'),
    from_addr TEXT, 
    to_addr TEXT,
    mail_subject TEXT,
    mail_content TEXT,
    mail_date TIMESTAMP,
    mail_status TEXT
);


INSERT INTO mail_storage(from_addr, to_addr, mail_subject, mail_content,mail_date, mail_status) VALUES
    ('eleve@ensai.fr', 'jean@insee.fr', 'RDV', 'Bonjour, a bientot', '2023-03-25 10:23:00', 'NOT_SENT'),
    ('truc@gmail.com', 'pierre@insee.fr', 'Re: Invit', 'Ok', '2023-02-20 17:53:00', 'SENT'),
    ('toto@outlook.com', 'eleve@insee.fr', '', 'Plus tard', '2005-03-25 11:12:00', 'NOT_SENT');