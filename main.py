import uvicorn

from server.background_checker import BackgroundChecker
from service.app import app

checker = BackgroundChecker()
checker.start()
uvicorn.run(app, host="127.0.0.1", port=8000)

#Launching both the API and the DB auto-checker.