import requests
import os
import dotenv
import datetime as dt
import tkinter as tk
from tkinter import ttk
from tkinter.messagebox import showinfo

from gui.mail_editor import EmailEditor


class HitoryEditWindow(tk.Toplevel): 
    """This is the window that pops up whenever the user selects a mail from the list"""

    def __init__(self, container, id_mail):
        super().__init__(container)
        self.container = container
        self.geometry('670x450+350+350')
        self.iconbitmap('./assets/mail.ico')
        dotenv.load_dotenv(override=True)
        self.result = requests.get(os.environ["WEBSERVICE_HOST"] + "demande/" + str(id_mail)).json()
        self.build_widgets()
        self.lift()

    def build_widgets(self):
        #Adapt text to the mail status
        window_title = "Modifier un envoi programmé" if self.result['mail_status'] == 'NOT_SENT_YET' else "Supprimer/Renvoyer un mail"
        delete_button_name = "Annuler l'envoi" if self.result['mail_status'] == 'NOT_SENT_YET' else "Supprimer"
        new_request_button_name = "Appliquer les modifications" if self.result['mail_status'] == 'NOT_SENT_YET' else "Renvoyer"
        if self.result['mail_status'] == 'NOT_SENT' : 
            showinfo(title = "Warning", message = "For some reason, this mail was not delivered")
        self.title(window_title)

        ###################################### Mail edit frame ########################################
        self.mail_edit = EmailEditor(self)
        self.mail_edit.init_fields(from_addr = self.result['from_addr'],
                                   to_addr = self.result['to_addr'],
                                   mail_subject= self.result['mail_subject'],
                                   mail_txt = self.result['mail_content'],
                                   mail_date = dt.datetime.strptime(self.result['mail_date'], '%Y-%m-%dT%H:%M:%S')) 
        ######################################## Buttons Frame ########################################
        buttons_frame = tk.Frame(self)
        ttk.Button(buttons_frame, 
                   text = "Annuler", 
                   command = self.destroy
                  ).grid(row = 0, column = 0)
        ttk.Button(buttons_frame, 
                   text = delete_button_name, 
                   command = lambda : self.delete_mail(self.result['id_mail'])
                  ).grid(row = 0, column = 1, padx = 7)
        ttk.Button(buttons_frame, 
                   text = new_request_button_name, 
                   command = lambda : self.new_scheduled_mail(self.result['id_mail'], self.result['mail_status'] == 'NOT_SENT_YET')
                  ).grid(row = 0, column = 2)
        ####################################### Frames packing #########################################
        self.mail_edit.pack()
        buttons_frame.pack(pady = 3)

    def delete_mail(self, mail_id, display_info = True):
        """Send a HTTP request to delete a mail"""

        result = requests.get(os.environ["WEBSERVICE_HOST"] + "demande/" + str(mail_id) + "/drop/").json()
        if display_info:
            showinfo(title = "Delete", message = result["status"])
        self.container.destroy()

    def new_scheduled_mail(self, mail_id, is_modification):
        """Send a HTTP request to schedule a new mail, after editting.

        If it is a modification on a pending mail, the former one will be deleted.

        Parameters
        ----------
        mail_id : int
        is_modification : bool
            Indicates whether it is a modification
        """
        self.mail_edit.check_user_entries()
        if is_modification:
            self.delete_mail(mail_id, False)
        self.container.destroy()
        



