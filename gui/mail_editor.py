import tkinter as tk
from tkinter import ttk
from tkinter.messagebox import showinfo, showwarning, askyesno
from tkcalendar import Calendar, DateEntry
import datetime as dt
import requests
import re

from gui.time_selector import TimeSelector


class EmailEditor(tk.Frame):
    """This is a frame that allows the user to type their mail

    It has all the needed fields : Sender and receiver addresses, subject, and text."""

    def __init__(self, container):
        super().__init__(container)
        self.build_widgets()
    
    def build_widgets(self):
        ############################# Adress frame ####################################
        address_frame = ttk.Frame(self)
        #--------------------------Widget definition-----------------------------------
        self.from_entry = ttk.Entry(address_frame, width = 42)
        self.to_entry = ttk.Entry(address_frame, width = 42)
        #---------------------------Widget position------------------------------------
        ttk.Label(address_frame, text = "From :").grid(row = 1, column = 1, padx = 5, pady = 5)
        ttk.Label(address_frame, text = "To : ").grid(row = 2, column = 1, padx = 5, pady = 5)
        self.from_entry.grid(row = 1, column = 2, padx = 5, pady = 5)
        self.to_entry.grid(row = 2, column = 2, padx = 5, pady = 5)
        
        ############################# Date Frame ######################################
        date_frame = ttk.Frame(self)
        #--------------------------Widget definition-----------------------------------
        self.time_selector = TimeSelector(date_frame)
        self.date_selector = DateEntry(date_frame, width = 20, bg = "darkblue", fg = "white", year = 2023)
        #---------------------------Widget position------------------------------------
        ttk.Label(date_frame, text = "Scheduled on :").grid(row = 0, column = 1, pady = 5)
        self.date_selector.grid(row = 0, column = 2, padx = 10)
        self.time_selector.grid(row = 0, column = 3)

        ############################## Middle frame ###################################
        middle_frame = ttk.Frame(self)
        #--------------------------Widget definition-----------------------------------
        self.subject_entry = ttk.Entry(middle_frame, width= 68)
        self.content = tk.Text(middle_frame, height = 12, width = 55)
        
        #---------------------------Widget position------------------------------------
        ttk.Label(middle_frame, text = "Subject :").grid(row = 1, column = 1, pady = 5)
        self.subject_entry.grid(row = 1, column = 2, pady = 5)
        ttk.Label(middle_frame, text = "Message :").grid(row = 2, column = 1)
        self.content.grid(row = 2, column = 2)

        ############################## Frame packing ##################################
        address_frame.pack()
        date_frame.pack()
        ttk.Separator(self, orient = "horizontal").pack(fill = 'x')
        middle_frame.pack(pady = 10)
        ttk.Separator(self, orient = "horizontal").pack(fill = 'x')
    
    def init_fields(self, from_addr, to_addr, mail_subject, mail_txt, mail_date):
        """Allows to initialize some or all the otherwise blank fields

        Params
        ------
        from_addr : str
        to_addr : str
        mail_subject : str
        mail_txt : str
        mail_date : datetime.datetime
        """
        self.from_entry.insert(0, from_addr)
        self.to_entry.insert(0, to_addr)
        self.subject_entry.insert(0, mail_subject)
        self.date_selector.set_date(mail_date.date())
        self.time_selector.set_time(mail_date.hour, mail_date.minute)
        self.content.insert('1.0', mail_txt)
    
    def reset_fields(self):
        self.from_entry.delete(0, 'end')
        self.to_entry.delete(0, 'end')
        self.subject_entry.delete(0, 'end')
        self.content.delete(1.0, 'end')

    def check_user_entries(self):
        mail_regex = "^[a-zA-Z]{1}[a-zA-Z0-9\-]+(?:\.[a-zA-Z0-9]+)*@[a-zA-Z0-9]+(?:\.[a-zA-Z0-9]+)+$"
        if re.fullmatch(mail_regex, self.from_entry.get()):
            #if sender email address is correct
            if re.fullmatch(mail_regex, self.to_entry.get()):
                #if receiver email address is correct
                complete_date = dt.datetime.combine(date = self.date_selector.get_date(),
                                                          time = self.time_selector.get_time())
                if complete_date > dt.datetime.now() + dt.timedelta(minutes=1):
                    #if the mail is scheduled in more than 1 minute
                    if self.subject_entry.get() == "":
                        #if there is no subject
                        if askyesno(title = "Confirmation", 
                                    message = "You did not specify the subject. Do you want \n to send it nonetheless?"):
                            self.send_mail_data(complete_date)
                    else:
                        self.send_mail_data(complete_date)
                else:
                    showwarning(title = "Warning", 
                            message = "You must schedule your mailing in 1 minutes from now at least:\nPlease enter a valid date")
            else:
                showwarning(title = "Warning", 
                            message = "Invalid email: {}".format(self.to_entry.get()))
        else:
            showwarning(title = "Warning", 
                            message = "Invalid email: {}".format(self.from_entry.get()))


    def send_mail_data(self, complete_date):
        """After verification, send a POST request to the API
        
        Once the mail is sent to the database, a message displays and the fields reset"""
        import os, dotenv
        dotenv.load_dotenv(override=True)
        requests.post(url = os.environ["WEBSERVICE_HOST"] + "demande/",
                      json = {"from_address" : self.from_entry.get(),
                              "to_address" : self.to_entry.get() ,
                              "mail_subject" : self.subject_entry.get() ,
                              "mail_text" : self.content.get(1.0, "end-1c"),
                              "desired_date" : str(complete_date)
                             }
                     )
        showinfo(title = "Mailing successfully scheduled",
                 message = "Your mail will be sent on " + complete_date.strftime('%B %d, %Y at %H:%M'))
        self.reset_fields()
