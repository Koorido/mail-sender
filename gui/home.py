import tkinter as tk
from tkinter import ttk

from gui.mail_viewer import MailViewer
from gui.mail_editor import EmailEditor


class Home(tk.Tk):
    """This is the first window that opens when the GUI is launched"""

    def __init__(self):
        super().__init__()
        self.title('Send your emails')
        self.geometry('670x450+250+250')
        self.resizable(0,0)
        self.iconbitmap('./assets/mail.ico')
        self.build_widgets()

    def build_widgets(self):
        ################### Mail Edit Frame ###################
        mail_edit = EmailEditor(self)
        #################### Buttons Frame ####################
        frame_bas = ttk.Frame(self)

        ttk.Button(frame_bas, 
                   text = "Historique", 
                   command = lambda : MailViewer(self)
                  ).pack(side = "left", padx = 15)

        ttk.Button(frame_bas, 
                   text = "Programmer l'envoi", 
                   command = mail_edit.check_user_entries
                  ).pack(side = "right", padx = 15)
        ################## Frames packing #####################
        mail_edit.pack()
        frame_bas.pack(pady = 5)
