import requests
import os
import dotenv
import tkinter as tk
import tkinter.font as tkFont
from tkinter import ttk

from gui.mail_editor import EmailEditor
from gui.history_window import HitoryEditWindow

class MailViewer(tk.Toplevel):
    """ This is the window that contains the mail list from history"""

    def __init__(self, container):
        super().__init__(container)
        dotenv.load_dotenv(override=True)
        self.iconbitmap('./assets/mail.ico')
        self.title("History")
        result = requests.get(os.environ["WEBSERVICE_HOST"] + "demandes/").json()
        self.mail_list =list(map((lambda dic: list(map(str,map(dic.__getitem__, ['id_mail', 'mail_subject', 'to_addr', 'mail_date'])))), result))
        # Cas d'application du meme sur la prog fonctionnelle. Mon API me renvoie une liste de dictionnaire, dont je veux recuperer
        #les memes values a chaque fois. Donc j'applique a cette liste une lambda-fonction qui prend un dict en entree et qui renvoie la
        # liste des values correspondant a la liste de keys que je passe en arg de map().
        self.build_widgets()

    def build_widgets(self):
        #Formatting strings and labels
        #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        cons10 = tkFont.Font(family='Consolas', size=10)  # Custom font (monospaced)
        labels = ["Id", "Objet", "Destinataire", "Le"]
        label_length = [2,5,12,2]
        max_length, display = [], []
        for i in range(4):
            max_length.append(max([len(mail[i]) for mail in self.mail_list]))
            #Determines and stores the widest string in each column

        for mail in self.mail_list:
            for i in range(4):
                mail[i] = mail[i].rjust(max(max_length[i], label_length[i]))
            display.append('   '.join(mail))

        for i in range(len(labels)) :
            labels[i] = labels[i].center(max(max_length[i], label_length[i]))
        self.window_width = int(9.5*len('   '.join(labels)+' '))
        self.geometry('{}x220+250+250'.format(self.window_width)) #Adapt window width
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        listbox_frame = tk.Frame(self)

        #---------------------Widget definition----------------------------
        ttk.Label(self, text = '   '.join(labels)+'  ', font = cons10).pack()
        ttk.Separator(self, orient="horizontal").pack(fill = 'x')
        self.listbox = tk.Listbox(master = listbox_frame, 
                                  listvariable=tk.Variable(value = display), 
                                  height=7, 
                                  width= int(self.window_width/9), #arbitrary
                                  selectmode=tk.SINGLE,
                                  font = cons10)

        self.scrollbar = ttk.Scrollbar(listbox_frame, orient=tk.VERTICAL, command=self.listbox.yview)
        self.listbox['yscrollcommand'] = self.scrollbar.set #link scrollbar and listbox
        self.listbox.bind('<<ListboxSelect>>', self.mail_selected) #call self.mailselected each time a list item is selected
        #---------------------Widget position------------------------------
        self.listbox.pack(side=tk.LEFT)
        self.scrollbar.pack(side=tk.RIGHT, expand=True, fill=tk.Y)
        listbox_frame.pack(expand = False)
        ttk.Separator(self, orient="horizontal").pack(fill = 'x')
        ttk.Button(self, text = "Retour", command = self.destroy).pack(pady = 3)
    
    def mail_selected(self, event):
        ID = self.listbox.get(self.listbox.curselection()[0]).split('   ')[0] #Get mail_id from displayed string
        HitoryEditWindow(self, int(ID)) #Opens editting window
        


