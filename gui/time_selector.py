import tkinter as tk
import datetime


class TimeSelector(tk.Frame):
    """Implements the spinboxes frame to select time"""

    def __init__(self, container):
        super().__init__(container)
        self.last_minute_value = ""
        self.build_widgets()

    def build_widgets(self):
        #Definition
        self.hour_var=tk.StringVar(self, '12')
        self.hour_select = tk.Spinbox(self, from_ = 0, to = 23, wrap = True, textvariable = self.hour_var, width = 2)
        self.min_var=tk.StringVar(self, '00')
        self.min_var.trace("w",self.trace_var)
        self.min_select = tk.Spinbox(self, from_ = 0, to = 59, wrap = True, textvariable = self.min_var, width = 2)

        #Placement
        self.hour_select.grid(row = 0, column = 0)
        tk.Label(self, text = "h").grid(row = 0, column = 1, padx = 2)
        self.min_select.grid(row = 0, column = 2)

    def trace_var(self, *args):
        """Allows the hour to update if minute digit goes from 0 to 59 or from 59 to 0"""
        if self.last_minute_value == "59" and self.min_var.get() == "0":
            self.hour_var.set(int(self.hour_var.get())+1 if self.hour_var.get() !="23" else 0)
        elif self.last_minute_value == "0" and self.min_var.get() == "59":
            self.hour_var.set(int(self.hour_var.get())-1 if self.hour_var.get() !="0" else 23)
        self.last_minute_value = self.min_var.get()

    def set_time(self, hour : str, minutes : str):
        self.hour_var.set(hour)
        self.min_var.set(minutes)

    def get_time(self):
        return datetime.time(hour = int(self.hour_var.get()), 
                             minute = int(self.min_var.get()))
