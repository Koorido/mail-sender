from gui.home import Home

try:
    from ctypes import windll
    windll.shcore.SetProcessDpiAwareness(1)
    #Trying to display high-resolution tkinter windows device-independantly
finally:
    Home().mainloop()