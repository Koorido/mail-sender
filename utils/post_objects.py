from pydantic import BaseModel

#These are two classes designed to receive json data sent with POST requests

class Mail(BaseModel):
    from_address : str
    to_address : str
    mail_subject : str
    mail_text : str
    desired_date : str 

class StatusUpdate(BaseModel):
    id_mail : int
    status_code : int