import threading
import time
import datetime
import requests
import os
import dotenv

from dao.mail_dao import MailDAO
from server.mail_sender import MailSender

class BackgroundChecker(threading.Thread):
    """A small programm that regularly checks for mail to send.

    Using threading in order to launch it along with the API
    (uvicorn itself uses threading internally )
    """
    def run(self,*args,**kwargs):
        dotenv.load_dotenv(override=True)
        time.sleep(10)  # Some delay to let the API start

        while True:
            current_time = datetime.datetime.now()
            next_minute = current_time + datetime.timedelta(minutes = 1, seconds=10)
            result = requests.get(os.environ["WEBSERVICE_HOST"] + "demandes/pending").json()
            for mail in result: #result could be empty

                if datetime.datetime.strptime(mail['mail_date'], '%Y-%m-%dT%H:%M:%S') < current_time:
                    #if it is too late to send the mail (e.g. the API has been shut down)
                    requests.post(url = os.environ["WEBSERVICE_HOST"] + "setstatus",
                                  json = {"id_mail" : int(mail['id_mail']), "status_code" : 0})
                    
                elif datetime.datetime.strptime(mail['mail_date'], '%Y-%m-%dT%H:%M:%S') < next_minute:
                    #if the mail is supposed to be send in less than 1 min 10
                    sender = MailSender(os.environ["SMTP_HOST"], os.environ["SMTP_PORT"], os.environ["SMTP_PASSWORD"])
                    #Initializes SMTP server, can fail if .env isn't completed
                    sender.send_mail(mail)
                    requests.post(url = os.environ["WEBSERVICE_HOST"] + "setstatus",
                                  json = {"id_mail" : int(mail['id_mail']), "status_code" : 1})
            time.sleep(59)