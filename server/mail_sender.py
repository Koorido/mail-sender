import smtplib
import ssl

class MailSender:
    """Defines a single-use mail sending object"""
    
    def __init__(self, smtp_server, port, passwd):
        self.context = ssl.create_default_context()
        self.server = smtplib.SMTP(smtp_server, port)
        self.passwd = passwd

    def send_mail(self, mail_json):
        self.server.starttls(context = self.context)
        self.server.login(mail_json['from_addr'], self.passwd)
        message = """\
Subject: {}

{}""" # It is important not to alter this formatting
        self.server.sendmail(mail_json['from_addr'], 
                             mail_json['to_addr'], 
                             message.format(mail_json['mail_subject'], mail_json['mail_content']),
                             mail_options = ['SMTPUTF8']) #Attempt to handle UTF-8 formatting... didn't succeed.
        self.server.quit()
